<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    public function __construct() {
		parent::__construct();
		
	}

    public function donatur()
    {
        $this->db->select('*');
        $this->db->where('qurban_type', 0);
        $data['data'] = $this->db->get('transaction')->result();
        
        $this->db->select_sum('gross_amount');
        $this->db->where('transaction_status', 'settlement');
        $this->db->where('qurban_type', 0);
        $data['total'] = $this->db->get('transaction')->row();

        echo json_encode($data);die;
    }

    public function mudhohi()
    {
        $this->db->select('*');
        $this->db->where('qurban_type', 1);
        $data['data'] = $this->db->get('transaction')->result();

        $this->db->select_sum('gross_amount');
        $this->db->where('transaction_status', 'settlement');
        $this->db->where('qurban_type', 1);
        $data['total'] = $this->db->get('transaction')->row();
        
        echo json_encode($data);die;
    }

}