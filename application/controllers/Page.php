<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {
    public function __construct() {
		parent::__construct();
        $this->load->model('MidtransModel', 'midtrans');
        $this->load->model('DonationModel', 'donation');
        $this->load->model('GeneralModel', 'general');
	}
	
	public function index() {
        // list of events
        $listEvents = $this->events->list();

        if (!empty($listEvents)) {
            $tmpEvents = array();
			foreach ($listEvents as $key => $row) {
				$param	        = array();
                $description    = '';
                if (!empty($row['description'])) {
                    $description            = $this->general->clearStringromTags($row['description']);
                    if (strlen($description) > 100) {
                        $description = substr($description, 0, 100) . ' .....';
                    }
                }
				$param['id']			= $row['id'];
				$param['title']			= $row['title'];
				$param['description']	= $description;
				if (empty($row['image']) || !is_file(str_replace(base_url(), '', $row['image'])))
					$row['image']			= base_url() . 'assets/images/cover.jpg';
				$param['image']			= $row['image'];
				$param['upload']		= date('d M Y H:i', strtotime($row['create_at']));
                $tmpEvents[]            = $param;
            }
            $data['events']		    = $tmpEvents;
        }
		$data['gallery']		= $this->gallery->list();
		$data['midtrans_key']	= $this->midtrans->clientKey;
		$this->twig->display('frontend/components/home_page', $data);
	}
	
	public function events($id) {
        $tmpData    = $this->general->fetchSingleData(array('id', $id), 'events');
        $param	    = array();
        if (!empty($tmpData)) {
            $param['title']			= $tmpData['title'];
            $param['description']	= $tmpData['description'];
            if (empty($tmpData['image']) || !is_file(str_replace(base_url(), '', $tmpData['image'])))
                $tmpData['image']			= base_url() . 'assets/images/cover.jpg';
            $param['image']			= $tmpData['image'];
            $param['upload']		= date('d M Y H:i', strtotime($tmpData['create_at']));
        }
        $data['data']   = $param;
		$this->twig->display('frontend/components/events', $data);
	}

    public function treeFamily() {
        //list of tree family
        $listFamily     = $this->family->fetchAllFamily();
        $tmpFamily      = array();
        if (!empty($listFamily)) {
            foreach ($listFamily as $key => $row) {
                $param          = array();
                $pid            = $row['father_id'];
                if (empty($row['father_id']))
                    $pid        = $row['mother_id'];
                $status         = $row['status_family'];
                if ($key > 0) {
                    $status     = 'Anak (' . $row['status_family'] . ')';
                }
                $param['id']    = $row['id'];
                $param['pid']   = $pid;
                $param['tags']  = array($status);
                $param['title'] = $status;
                $param['name']  = $row['name'];
				if (empty($row['image']) || !is_file(str_replace(base_url(), '', $row['image'])))
					$row['image']			= base_url() . 'assets/images/user.png';
                $param['img']   = $row['image'];
                $tmpFamily[]    = $param;
            }
        }

        $data['family']         = json_encode($tmpFamily);
		$this->twig->display('frontend/components/tree_family', $data);
    }

    public function donation($secret_key = null) {
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'transaksi');
        $count = $this->donation->getDonationCount();
        $persen = @($count / 100 * 100); 
        $data['total'] = $count;
        $data['minus'] = @(100 - $count);
        $data['percen'] = round($persen,2);
        $last   = $this->donation->last_data_get();
        if(empty($last)){
            $data['last']   = 1;
        }else{
            $data['last']   = $last->id + 1;
        }
		$this->twig->display('frontend/components/donation', $data);
    }

    public function beranda($secret_key = null) {
        if(!empty($secret_key)){
            $param = array();
            $param['secret_key'] = $secret_key;
            $param['created'] = date('Y-m-d H:i:s');
            $this->general->insertData('secret_history', $param);
        }
        $param['secreet'] = $secret_key;

        $param = array();
        $param['value'] = 1;
        $this->db->insert("count_invitation", $param);
        
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'beranda');
        $data['data'] = $this->general->retrieveAll('galery');
		$this->twig->display('frontend/components/beranda', $data);
    }

    public function mudhohi($secret_key = null) {
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'mudhohi');
        $count = $this->donation->getDonationCount();
        $persen = @($count / 100 * 100); 
        $data['total'] = $count;
        $data['minus'] = @(100 - $count);
        $data['percen'] = round($persen,2);
        $data['data'] = $this->donation->getDonationUsers();
        $nominal = $this->donation->getDonationUsersMudhohi();
        $mudhohi = $this->donation->getDonationUsersAccept(1);
        $donatur = $this->donation->getDonationUsersAccept(0);
        $data['nominal'] = $nominal->gross_amount;
        $data['mudhohi'] = $mudhohi;
        $data['donatur'] = $donatur;
		$this->twig->display('frontend/components/mudhohi', $data);
    }

    public function daerah($secret_key = null) {
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'daerah');
        $count = $this->donation->getDonationCount();
        $persen = @($count / 100 * 100); 
        $data['total'] = $count;
        $data['minus'] = @(100 - $count);
        $data['percen'] = round($persen,2);
        $data['data'] = $this->general->retrieveMustahiq('umum_kurban');
        $this->twig->display('frontend/components/sebaran', $data);
    }
    public function kalkulator($secret_key = null) {
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'kalkulator');
        $count = $this->donation->getDonationCount();
        $persen = @($count / 100 * 100); 
        $data['total'] = $count;
        $data['minus'] = @(100 - $count);
        $data['percen'] = round($persen,2);
        $this->twig->display('frontend/components/kalkulator', $data);
    }
    public function kontak($secret_key = null) {
        $this->twig->addGlobal('secret_key', $secret_key);
        $this->twig->addGlobal('menu', 'kontak');
        $count = $this->donation->getDonationCount();
        $persen = @($count / 100 * 100); 
        $data['total'] = $count;
        $data['minus'] = @(100 - $count);
        $data['percen'] = round($persen,2);
        $this->twig->display('frontend/components/kontak', $data);
    }
}
